package com.company;

import java.util.ArrayList;

/**
 * Created by Alexander on 22.08.2017.
 * Class for the moving horse
 */
class Mover {
    //current position
    int[] position;
    //number of moves so far
    int numberOfMoves;
    //List of moves so it can be printed later
    ArrayList<Integer[]> moves;

    Mover(int[] position, int numberOfMoves) {
        moves = new ArrayList<>();
        this.position = position;
        this.numberOfMoves = numberOfMoves;
        moves.add(new Integer[] {position[0], position[1]});
    }

    Mover(Mover mover) {
        moves = new ArrayList<>(mover.moves);
        this.position = new int[2];
        this.position[0] = mover.position[0];
        this.position[1] = mover.position[1];
        this.numberOfMoves = mover.numberOfMoves;
    }

    Mover getMove(boolean isDown, boolean isRight, boolean isFirstLong) {
        //pretty straightforward approach:
        //if isDown == true- the horse goes down. Otherwise- up
        //if isRight == true- the horse goes right. Otherwise- left
        //if isFirstLong == true- than the move for 2 cells will be vertical (Y-axis). Else- horizontal (X-axis)
        //E.g.: getMove(true, true, true) will move the horse 2 cells down and 1 cell right
        //E.g.: getMove(false, true, false) will move the horse 1 cell up and 2 cells right
        if (isFirstLong) {
            if (isDown) {
                position[1] += 2;
                if (isRight)
                    position[0] += 1;
                else
                    position[0] -= 1;
            } else {
                position[1] -= 2;
                if (isRight)
                    position[0] += 1;
                else
                    position[0] -= 1;
            }

        } else {
            if (isDown) {
                position[1] += 1;
                if (isRight)
                    position[0] += 2;
                else
                    position[0] -= 2;
            } else {
                position[1] -= 1;
                if (isRight)
                    position[0] += 2;
                else
                    position[0] -= 2;
            }
        }

        if (position[0] < 1 || position[0] > 8 || position[1] < 1 || position[1] > 8)
            //horse falls off the board, so return big number and don't use it in next call of recursive function
            numberOfMoves = 100;
        else {
            numberOfMoves++;
            moves.add(new Integer[] {position[0], position[1]});
        }

        return this;
    }

    static Mover compare(Mover mover1, Mover mover2) {
        //returning mover with less moves so far
        //if both of them are "null"- return null.
        //if one of them is null- return the other one.
        //Otherwise- return the least

        if (mover1 == null && mover2 == null)
            return null;
        if (mover1 == null)
            return mover2;
        if (mover2 == null)
            return mover1;

        if (mover1.numberOfMoves < mover2.numberOfMoves)
            return mover1;

        return mover2;
    }
}