package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //starting position
        int startX = 1;
        int startY = 1;

        //ending position
        int endX = 8;
        int endY = 8;

        Mover mover = new Mover(new int[] {startX, startY}, 0);
        Mover bestMover = bestMoves(mover, endX, endY);

        System.out.println("Best moves: " + bestMover.numberOfMoves);

        System.out.println();
        Drawer.drawGrid(startX, startY, startX, startY);

        Integer[] prevPosition = null;
        for (Integer[] move: bestMover.moves) {
            if (prevPosition != null) {
                System.out.println();
                System.out.println("(" + move[0] + "-" + move[1] + ")");
                Drawer.drawGrid(prevPosition[0], prevPosition[1], move[0], move[1]);
            }

            prevPosition = move;
        }
    }

    private static Mover bestMoves(Mover mover, int endX, int endY) {

        //maximum number of moves before branch is terminated
        int moveNumber = 10;

        //return current mover if we are already in a final position- no further moves are needed
        if (mover.position[0] == endX && mover.position[1] == endY)
            return mover;

        if (mover.numberOfMoves < moveNumber) {
            //creating array. length = 8, so we can keep every possible move for the horse (disregarding board edges for now)
            Mover[] moversArray = new Mover[8];

            for (int i = 1; i < 9; i++) {
                //need to get movers with all possible boolean conditions: fff, fft, ftf, ftt, tff, tft, ttf, ttt
                //Using the given loop, we will have the first boolean to be "false" for the first 4 cycles and become "true" for the last 4 cycles
                //Second boolean will swap its value every 2 cycles
                //Third boolean will swap its value every swap
                moversArray[i - 1] = new Mover(mover).getMove(i > 4, (i % 4 == 0 || (i + 1) % 4 == 0), i % 2 == 0);
            }

            Mover min = null;

            for (Mover currentMover: moversArray) {
                if (currentMover.numberOfMoves < moveNumber)

                    //recursive call of "bestMoves" will return the shortest path from current cell to final for each move, that was calculated earlier.
                    //We will keep the best of those moves in "min"
                    min = Mover.compare(min, bestMoves(currentMover, endX, endY));
            }

            return min;
        }

        return mover;
    }





}
