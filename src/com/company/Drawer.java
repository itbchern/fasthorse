package com.company;

/**
 * Created by Alexander on 22.08.2017.
 * Class for drawing the moves of the horse
 */
class Drawer {
    private static void drawCell(boolean isEndCell, boolean isStartCell) {
        //empty square
        char ch = 11036;

        if (isStartCell)
            //filled square
            ch = 11035;
        if (isEndCell)
            //filled circle
            ch = 11044;

        System.out.print("\t" + ch);
    }

    static void drawGrid(int startX, int startY, int endX, int endY) {
        System.out.println("\t1\t2\t3\t4\t5\t6\t7\t8");

        for (int i = 1; i < 9; i++) {
            System.out.print(i);
            for (int j = 1; j < 9; j++) {
                if (j == endX && i == endY) {
                    Drawer.drawCell(true, false);
                    continue;
                }

                if (j == startX && i == startY) {
                    Drawer.drawCell(false, true);
                    continue;
                }
                    Drawer.drawCell(false, false);
            }

            System.out.println();
        }
    }
}
